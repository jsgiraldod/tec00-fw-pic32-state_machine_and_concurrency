#include "main.h"

void interrupt_init(void)
{
		/* Configure the interruption of the core timer */
    mConfigIntCoreTimer((CT_INT_ON | CT_INT_PRIOR_7 | CT_INT_SUB_PRIOR_0));
	
	/* Configure and enable the interrupts of the system */
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
}

void __ISR(_CORE_TIMER_VECTOR, IPL7_AUTO) interrupt_core_timer_handler(void)
{
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);

   core_timer_increment_sys_tick();
}
