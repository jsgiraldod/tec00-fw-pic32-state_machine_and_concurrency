/* 
 * File:   main.c
 * Author: user8
 *
 * Created on July 24, 2021, 8:54 AM
 */
#include "main.h"
#include "config_bits.h"

#define HEARTBEAT_PERIOD 500 // ms
volatile uint32_t sys_tick = 0;

void gpio_init(void);
void interrupt_init(void);

void heartbeat_handler(void);

/*
 * 
 */
void main(void) {

	/* Set the period of the core timer to 1ms */
    OpenCoreTimer(CORE_TICK_RATE);
	
	gpio_init();
	interrupt_init();
	
	while (1) {
		heartbeat_handler();
	}
}

void heartbeat_handler(void) 
{
    static uint32_t heartbeat_tick = 0;
    
    if ((core_timer_get_sys_tick() - heartbeat_tick) > HEARTBEAT_PERIOD) {
        heartbeat_tick = core_timer_get_sys_tick();
        mPORTEToggleBits(LED_1);
    }
}

void gpio_init(void)
{ 
	mPORTESetPinsDigitalOut(LED_1 | LED_2 | LED_3);
    mPORTEClearBits(LED_1 | LED_2 | LED_3);
}