/* 
 * File:   main.h
 * Author: user8
 *
 * Created on July 24, 2021, 8:54 AM
 */

#ifndef MAIN_H
#define	MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "xc.h"
#include "plib.h"

#include "core_timer.h"

#define SYSCLK 96000000
#define FPBCLK (SYSCLK / 1)
#define CORE_TICK_RATE (SYSCLK / 2 / 1000) // 1ms

#define LED_R BIT_10
#define LED_G BIT_3
#define LED_B BIT_7
#define LED_1 BIT_4
#define LED_2 BIT_6
#define LED_3 BIT_7


#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

