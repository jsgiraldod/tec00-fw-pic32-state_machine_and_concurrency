/* 
 * File:   core_timer.h
 * Author: user8
 *
 * Created on July 23, 2021, 9:55 AM
 */

#ifndef CORE_TIMER_H
#define	CORE_TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
    
extern volatile uint32_t sys_tick;

inline void core_timer_increment_sys_tick(void) {
    sys_tick++;
}

inline uint32_t core_timer_get_sys_tick(void) {
    return sys_tick;
}



#ifdef	__cplusplus
}
#endif

#endif	/* CORE_TIMER_H */

